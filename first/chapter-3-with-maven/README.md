# Подготовка

Добавление через maven jdbc драйвера для работы с postgresql

# Содержание

1. Создаём базу данных в СУБД PostgreSQL.
2. Создаем запросы для создания таблиц, описывающих модели Player, Team.
3. Разрабатываем методы для работы с СУБД.

