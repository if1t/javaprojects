CREATE TABLE IF NOT EXISTS team
(
    id INTEGER GENERATED BY DEFAULT AS IDENTITY
        CONSTRAINT team_pk
            PRIMARY KEY,
    name TEXT,
    description TEXT
);
comment ON TABLE team is 'Команда';
