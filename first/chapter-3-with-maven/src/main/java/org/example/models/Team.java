package org.example.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Команда */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class Team {

    /** Идентификатор команды */
    private Long id;
    /** Название команды */
    private String name;
    /** Описание команды */
    private String description;

    public void PrintInfo() {
        System.out.println("Название: ".concat(name));
        System.out.println("Описание: ".concat(description));
    }
}
