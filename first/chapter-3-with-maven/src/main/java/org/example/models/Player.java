package org.example.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Профиль пользователя */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class Player {

    /** Идентификатор профиля */
    private Long id;
    /** Никнейм */
    private String nickname;
    /** Название команды игрока */
    private String team;
    /** Позиция в игре */
    private String position;

    public void printInfo() {
        System.out.println("Ник: ".concat(nickname));
        System.out.println("Команда: ".concat(team));
        System.out.println("Позиция: ".concat(position));
    }
}
