package org.example;

import org.example.models.Player;
import org.example.models.Team;
import org.example.orm.DbProvider;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        DbProvider db = new DbProvider("jdbc:postgresql://localhost:5432/chapter-3", "postgres", "");
        db.createTablesIfNotExists();

        /*
        List<Team> teams = CsvParser.parseTeams("first/chapter-2/data/teams.csv");
        db.insertAllTeams(teams);

        List<Player> players = CsvParser.parsePlayers("first/chapter-2/data/players.csv");
        db.insertAllPlayers(players);
        */

        List<Team> foundTeams = db.findAllTeams();
        List<Player> foundPlayers = db.findAllPlayers();
        List<Player> foundPlayersByTeamLiquid = db.findAllPlayersByTeam("Team Liquid");
        Player foundPlayerById1 = db.findPlayerById(1);
    }
}