package org.example.orm;

import org.example.models.Player;
import org.example.models.Team;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DbProvider {
    public String url;
    public String user;
    public String password;

    private Connection connection;

    public DbProvider(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;

        connect();
    }

    private void connect() {
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createTablesIfNotExists() {
        executeSqlFile("first/chapter-3-with-maven/target/classes/queries/create-table-team.sql");
        executeSqlFile("first/chapter-3-with-maven/target/classes/queries/create-table-player.sql");
    }

    private void executeSqlFile(String filePath) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            Statement statement = connection.createStatement();
            StringBuilder sqlBuilder = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                if (!line.trim().isEmpty() && !line.startsWith("--")) {
                    sqlBuilder.append(line);
                    if (line.endsWith(";")) {
                        String sql = sqlBuilder.toString();
                        sqlBuilder.setLength(0);

                        statement.executeUpdate(sql);
                    }
                }
            }

            // Проверка, что запрос полностью выполнился
            if (sqlBuilder.length() > 0) {
                String sql = sqlBuilder.toString();
                statement.executeUpdate(sql);
            }
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertAllTeams(List<Team> teams) {
        final String sql = "INSERT INTO team (name, description) VALUES (?, ?)";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            for (Team team : teams) {
                preparedStatement.setString(1, team.getName());
                preparedStatement.setString(2, team.getDescription());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to insert teams", e);
        }
    }

    public void insertAllPlayers(List<Player> players) {
        final String sql = "INSERT INTO player (nickname, team, position) VALUES (?, ?, ?)";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            for (Player player : players) {
                preparedStatement.setString(1, player.getNickname());
                preparedStatement.setString(2, player.getTeam());
                preparedStatement.setString(3, player.getPosition());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to insert players", e);
        }
    }

    public List<Team> findAllTeams() {
        List<Team> teams = new ArrayList<>();
        final String sql = "SELECT id, name, description FROM team";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Team team = Team.builder()
                        .id(resultSet.getLong("id"))
                        .name(resultSet.getString("name"))
                        .description(resultSet.getString("description"))
                        .build();
                teams.add(team);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to find all teams", e);
        }

        return teams;
    }

    public List<Player> findAllPlayers() {
        List<Player> players = new ArrayList<>();
        final String sql = "SELECT id, nickname, team, position FROM player";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Player player = Player.builder()
                        .id(resultSet.getLong("id"))
                        .nickname(resultSet.getString("nickname"))
                        .team(resultSet.getString("team"))
                        .position(resultSet.getString("position"))
                        .build();
                players.add(player);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to find all players", e);
        }

        return players;
    }

    public List<Player> findAllPlayersByTeam(String teamName) {
        List<Player> players = new ArrayList<>();
        final String sql = "SELECT id, nickname, team, position FROM player WHERE team = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, teamName);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Player player = Player.builder()
                        .id(resultSet.getLong("id"))
                        .nickname(resultSet.getString("nickname"))
                        .team(resultSet.getString("team"))
                        .position(resultSet.getString("position"))
                        .build();
                players.add(player);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to find players by team", e);
        }

        return players;
    }

    public Player findPlayerById(int playerId) {
        final String sql = "SELECT id, nickname, team, position FROM player WHERE id = ?";
        Player foundPlayer = null;

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, playerId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                foundPlayer = Player.builder()
                        .id(resultSet.getLong("id"))
                        .nickname(resultSet.getString("nickname"))
                        .team(resultSet.getString("team"))
                        .position(resultSet.getString("position"))
                        .build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to find player by id", e);
        }

        return foundPlayer;
    }
}
