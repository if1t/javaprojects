package models;

import java.util.List;

/** Команда */
public class Team {

    /** Идентификатор команды */
    private Long id;

    /** Название команды */
    private String name;

    /** Описание команды */
    private String description;

    public Team(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }
}
