import models.Player;
import models.Team;
import operations.CsvParser;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Team> teams = CsvParser.parseTeams("first/chapter-2/data/teams.csv");
        List<Player> players = CsvParser.parsePlayers("first/chapter-2/data/players.csv");

        System.out.println("Команды:");
        teams.forEach(Team::PrintInfo);
        System.out.println("\nИгроки:");
        players.forEach(Player::PrintInfo);
    }
}