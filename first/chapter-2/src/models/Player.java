package models;

/** Профиль пользователя */
public class Player {

    /** Идентификатор профиля */
    private Long id;
    /** Никнейм */
    private String nickname;
    /** Название команды игрока */
    private String nameTeam;
    /** Позиция в игре */
    private String position;

    public Player(Long id, String nickname, String nameTeam, String position) {
        this.id = id;
        this.nickname = nickname;
        this.nameTeam = nameTeam;
        this.position = position;
    }

    public void PrintInfo() {
        System.out.println("Ник: ".concat(nickname));
        System.out.println("Команда: ".concat(nameTeam));
        System.out.println("Позиция: ".concat(position));
    }
}
