package models;

/** Команда */
public class Team {

    /** Идентификатор команды */
    private Long id;
    /** Название команды */
    private String name;
    /** Описание команды */
    private String description;

    public Team(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public void PrintInfo() {
        System.out.println("Название: ".concat(name));
        System.out.println("Описание: ".concat(description));
    }
}
