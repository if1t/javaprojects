package operations;

import models.Player;
import models.Team;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CsvParser {
    public static List<Team> parseTeams(String filename) {
        List<Team> teams = new ArrayList<>();
        Long idTeam = 0L;

        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            boolean firstLine = true;
            while ((line = br.readLine()) != null) {
                if (firstLine) {
                    firstLine = false;
                    continue;
                }
                String[] data = line.split(";");
                if (data.length >= 2) {
                    String name = data[0].trim();
                    String description = data[1].trim();

                    teams.add(new Team(idTeam++, name, description));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return teams;
    }

    public static List<Player> parsePlayers(String filename) {
        List<Player> players = new ArrayList<>();
        Long idPlayer = 0L;

        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            boolean firstLine = true;
            while ((line = br.readLine()) != null) {
                if (firstLine) {
                    firstLine = false;
                    continue;
                }
                String[] data = line.split(",");
                if (data.length >= 3) {
                    String nickname = data[0].trim();
                    String nameTeam = data[1].trim();
                    String position = data[2].trim();

                    players.add(new Player(idPlayer++, nickname, nameTeam, position));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return players;
    }
}
