package org.example.chapter4spring.repository;

import org.example.chapter4spring.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepo extends JpaRepository<Team, Long>, JpaSpecificationExecutor<Team> {
}
