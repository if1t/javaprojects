package org.example.chapter4spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Chapter4SpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(Chapter4SpringApplication.class, args);
	}

}
