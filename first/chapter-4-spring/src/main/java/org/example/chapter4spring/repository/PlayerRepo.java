package org.example.chapter4spring.repository;

import org.example.chapter4spring.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepo extends JpaRepository<Player, Long>, JpaSpecificationExecutor<Player> {
}
